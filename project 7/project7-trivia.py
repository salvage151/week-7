#Liam Wilson
#10/7/15
#Project 7 Trivia
import trivia

def q1():
	c = 0
	print("It's Player 1's Turn")
	Trivia = trivia.Trivia("What continent has no active volcanoes?","Asia","North America","South America","Australia")
	print("Question",Trivia.question())
	print("1.", Trivia.ans1())
	print("2.", Trivia.ans2())
	print("3.", Trivia.ans3())
	print("4.", Trivia.ans4())
	ans = int(input("What is the answer? "))
	if ans == 4:
		print("You're Correct")
		c += 1
	else:
		print("You're wrong, loser")
		print("The correct answer is: ",Trivia.ans4())


	Trivia = trivia.Trivia("What is the fourth largest state?","Texas","North Dakota","Montana","Nevada")
	print("Question",Trivia.question())
	print("1.", Trivia.ans1())
	print("2.", Trivia.ans2())
	print("3.", Trivia.ans3())
	print("4.", Trivia.ans4())
	ans = int(input("What is the answer? "))
	if ans == 3:
		print("You're Correct")
		c += 1
	else:
		print("You're wrong, loser")
		print("The correct answer is: ",Trivia.ans3())

	Trivia = trivia.Trivia("Which Roman Emperor had a name meaning Little Boots?","Caligula","Nero","Caesar","Augustus")
	print("Question",Trivia.question())
	print("1.", Trivia.ans1())
	print("2.", Trivia.ans2())
	print("3.", Trivia.ans3())
	print("4.", Trivia.ans4())
	ans = int(input("What is the answer? "))
	if ans == 1:
		print("You're Correct")
		c += 1
	else:
		print("You're wrong, loser")
		print("The correct answer is: ",Trivia.ans1())

	Trivia = trivia.Trivia("What name is the fruit Laraha more commanly known as?","Apple","Orange","Pear","Date")
	print("Question",Trivia.question())
	print("1.", Trivia.ans1())
	print("2.", Trivia.ans2())
	print("3.", Trivia.ans3())
	print("4.", Trivia.ans4())
	ans = int(input("What is the answer? "))
	if ans == 2:
		print("You're Correct")
		c += 1
	else:
		print("You're wrong, loser")
		print("The correct answer is: ",Trivia.ans2())

	Trivia = trivia.Trivia("What is the first medication to be sold in a water soluble tablet?","Aclaseltzer","Ibuprofen","Aspirin","Tumz")
	print("Question",Trivia.question())
	print("1.", Trivia.ans1())
	print("2.", Trivia.ans2())
	print("3.", Trivia.ans3())
	print("4.", Trivia.ans4())
	ans = int(input("What is the answer? "))
	if ans == 3:
		print("You're Correct")
		c += 1
	else:
		print("You're wrong, loser")
		print("The correct answer is: ",Trivia.ans3())



	print("Player 1,You got", c, "out of 5")
	print((c / 5) * 100,"%")

def q2():
	b = 0
	print("Player 2's Turn")
	Trivia = trivia.Trivia("What member of the cat family cannot retract its claws?","Panther","Cheetah","Tiger","Lion")
	print("Question",Trivia.question())
	print("1.", Trivia.ans1())
	print("2.", Trivia.ans2())
	print("3.", Trivia.ans3())
	print("4.", Trivia.ans4())
	ans = int(input("What is the answer? "))
	if ans == 2:
		print("You're Correct")
		b += 1
	else:
		print("You're wrong, loser")
		print("The correct answer is: ",Trivia.ans2())

	Trivia = trivia.Trivia("What was the first planet to be discovered by telescope?","Uranus","Mercury","Neptune","Pluto")
	print("Question",Trivia.question())
	print("1.", Trivia.ans1())
	print("2.", Trivia.ans2())
	print("3.", Trivia.ans3())
	print("4.", Trivia.ans4())
	ans = int(input("What is the answer? "))
	if ans == 1:
		print("You're Correct")
		b += 1
	else:
		print("You're wrong, loser")
		print("The correct answer is: ",Trivia.ans1())

	Trivia = trivia.Trivia("What decade gave us the ATM?","60s","80s","90s","70s")
	print("Question",Trivia.question())
	print("1.", Trivia.ans1())
	print("2.", Trivia.ans2())
	print("3.", Trivia.ans3())
	print("4.", Trivia.ans4())
	ans = int(input("What is the answer? "))
	if ans == 4:
		print("You're Correct")
		b += 1
	else:
		print("You're wrong, loser")
		print("The correct answer is: ",Trivia.ans4())

	Trivia = trivia.Trivia("In what country were police dogs first used?","Scotland","Ireland","Germany","United States")
	print("Question",Trivia.question())
	print("1.", Trivia.ans1())
	print("2.", Trivia.ans2())
	print("3.", Trivia.ans3())
	print("4.", Trivia.ans4())
	ans = int(input("What is the answer? "))
	if ans == 1:
		print("You're Correct")
		b += 1
	else:
		print("You're wrong, loser")
		print("The correct answer is: ",Trivia.ans1())


	Trivia = trivia.Trivia("Retsina wine is made in what country?","Russia","France","Greece","Germany")
	print("Question",Trivia.question())
	print("1.", Trivia.ans1())
	print("2.", Trivia.ans2())
	print("3.", Trivia.ans3())
	print("4.", Trivia.ans4())
	ans = int(input("What is the answer? "))
	if ans == 3:
		print("You're Correct")
		b += 1
	else:
		print("You're wrong, loser")
		print("The correct answer is: ",Trivia.ans3())


	print("Player 2,You got", b, "out of 5")
	print((b / 5) * 100,"%")


q1()
q2()
